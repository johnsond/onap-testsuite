*** Settings ***
Documentation	  Testing asdc.
Library    OperatingSystem
Library    RequestsLibrary
Library    Collections
Library 	      ExtendedSelenium2Library
Resource          ../resources/test_templates/model_test_template.robot

Test Template         Model Distribution For Directory
Test Teardown    Teardown Model Distribution

*** Variables ***

*** Test Cases ***
Distribute vLB Model    vLB
    [Tags]    distribute   distributeVLB
Distribute vFW Model    vFW
    [Tags]    distribute
Distribute vVG Model    vVG
    [Tags]    distribute
Distribute vFWDT Model    vFWDT
    [Tags]    distributeVFWDT
